#Front-end

Перед началом работы проверить все ли грант модули нужны, ненужные удалить из package.json и Gruntfile.js
Запустить npm install и bower install
Bower использую для иконочного шрифта "fontawesome", грант модулем "grunt-contrib-copy" копирую нужные файлы в проект.
Убедиться, что директории "node_modules", "bower_components" и ".sass-cache" находятся в гитигноре.

#Backend
Убрать все ненужные на проекте настройки темы, которые есть на странице admin/appearance/settings/boron. 
Сделать это нужно в файле theme-settings.php, .info файле и в шаблонах, где они используются.
Например, если не нужны бредкрамбы, нужно удалить из theme-settings.php, boron.info все настройки связанные с ними и в шаблоне page.tpl.php удалить код
<?php if ($breadcrumb): ?>
    <div id="breadcrumb"><?php print $breadcrumb; ?></div>
<?php endif; ?>
