module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    compass: true
                },
                files: {
                    'css/application.css': 'scss/application.scss'
                }
            }
        },

        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [
                    // Include your own custom scripts (located in the custom folder)
                    'js/vendor/*.js',
                    'js/custom/*.js'

                ],
                // Finally, concatinate all the files above into one single file
                dest: 'js/applications.js',
            },
        },

        uglify: {
            dist: {
                files: {
                    // Shrink the file size by removing spaces
                    'js/applications.js': ['js/applications.js']
                }
            }
        },

        svgstore: {
            options: {
                cleanupdefs: true
            },
            default : {
                files: {
                    'images/defs.svg': ['images/svgs/*.svg']
                }
            }
        },

        svginjector: {
            svgdefs: {
                options: {
                    container: 'svgPlaceholder'
                },
                files: {
                    'js/custom/svgdefs.js': 'images/defs.svg'
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'images/',
                }],
            },
        },

		copy: {
			font_awesome_fonts: {
				expand: true,
				cwd: 'bower_components/fontawesome/fonts',
				src: ['**'],
				dest: 'fonts/fontawesome/'
			},
			font_awesome_scss: {
				expand: true,
				cwd: 'bower_components/fontawesome/scss',
				src: ['**'],
				dest: 'scss/setup/fontawesome'
			},

		},

		'string-replace': {

			fontawesome: {
				files: {
					'scss/setup/fontawesome/_variables.scss': 'scss/setup/fontawesome/_variables.scss'
				},
				options: {
					replacements: [
						{
							pattern: '../fonts',
							replacement: '../fonts/fontawesome'
						}
					]
				}
			},
		},

        watch: {
            sass: {
                files: 'scss/**/*.scss',
                tasks: ['sass'],
            },
            js: {
                files: 'js/custom/**/*.js',
                tasks: ['concat', 'uglify'],
            },
            svgstore: {
                files: 'images/svgs/*.svg',
                tasks: ['svgstore', 'svginjector'],
            },
            imagemin: {
                files: 'images/*.{png,jpg,gif}',
                tasks: ['imagemin'],
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-svginjector');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-string-replace');

    grunt.registerTask('default', ['sass', 'concat', 'uglify', 'svgstore', 'svginjector', 'imagemin', 'copy', 'string-replace:fontawesome']);
};