# Require any additional compass plugins here.
add_import_path "bower_components/foundation/scss"
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"
# by default fonts_dir = css_dir/fonts
fonts_dir = "fonts"

# The environment mode. Defaults to :production, can also be :development
environment = :production

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = (environment == :production) ? :compressed : :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# Indicates whether line comments should be added to compiled css that says where the selectors were defined.
# Defaults to false in production mode, true in development mode.
# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

preferred_syntax = :scss

# Enable css sourse map http://www.sitepoint.com/using-source-maps-debug-sass-chrome/
sass_options = {:sourcemap => true}

# this one for switching between production and dev versions of the website
# example for use:
#   .button { background: url( image_path('img.png') ); }
# this will return you:
#   .button { background: url('../images/img.png'); }

module Sass::Script::Functions
  def image_path(string)
    assert_type string, :String
    Sass::Script::String.new("../images/#{string.value}")
  end
  alias_method :"image-path",:image_path 
  declare :"image-path", :args => [:string]
end
